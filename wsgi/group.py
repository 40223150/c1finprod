    def printsorteddata(self, ordermethod=0):
        檔案 = open(data_dir+"mydata_w.txt", "r", encoding="utf-8")
        數列 = []
        for 行資料 in 檔案:
            學號, 姓名, 尾碼 = 行資料.strip().split(",")
            數列.append([學號, 姓名, 尾碼])
        檔案.close()
        # 以原先數列為輸入, 使用 lamda 內置函式, 拉出資料的第三位(也就是尾碼)轉為整束後排序, 且由小到大
        # 若  reverse = True 則由大到小反排
        數列 = sorted(數列, key=lambda data: int(data[2]), reverse=int(ordermethod))
        outstring = ""
        組序 = 1
        current_pivot = 數列[0][2]
        for 索引 in range(len(數列)):
            if 數列[索引][2] == current_pivot:
                數列[索引].append(str(組序))
            else: 
                組序 += 1
                current_pivot = 數列[索引][2]
                數列[索引].append(str(組序))
            outstring += 數列[索引][0] + ",尾碼"+數列[索引][2]+",第"+數列[索引][3]+"組<br />"
        return outstring+"<br />"+self.mymenu()